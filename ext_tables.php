<?php
(defined('TYPO3_MODE') || defined('TYPO3')) || die('Access denied [kitt3n_layouts ext_table.php].');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('kitt3n_layouts', 'Configuration/TypoScript', 'kitt3n | Layouts');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder