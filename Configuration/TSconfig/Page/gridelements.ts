tx_gridelements.setup {
    1 {
        title = Grid 1 Spalte
        description = Fügt ein Grid-Element mit 1 Spalte ein
        frame = 3
        topLevelLayout = 0
        config {
            colCount = 1
            rowCount = 1
            rows {
                1 {
                    columns {
                        1 {
                            name = Inhalt
                            colPos = 10
                        }
                    }
                }
            }
        }
        icon = EXT:kitt3n_layouts/Resources/Public/Icons/GridElements/ge_1col.png
        flexformDS = FILE:EXT:kitt3n_layouts/Configuration/FlexForm/GridElements/1column.xml
    }

    2 {
        title = Grid 2 Spalten
        description = Fügt ein Grid-Element mit 2 Spalten ein
        frame = 3
        topLevelLayout = 0
        config {
            colCount = 2
            rowCount = 1
            rows {
                1 {
                    columns {
                        1 {
                            name = Links
                            colPos = 10
                        }
                        2 {
                            name = Rechts
                            colPos = 11
                        }
                    }
                }
            }
        }
        icon = EXT:kitt3n_layouts/Resources/Public/Icons/GridElements/ge_2col.png
        flexformDS = FILE:EXT:kitt3n_layouts/Configuration/FlexForm/GridElements/2columns.xml
    }

    3 {
        title = Grid 3 Spalten
        description = Fügt ein Grid-Element mit 3 Spalten ein
        frame = 3
        topLevelLayout = 0
        config {
            colCount = 3
            rowCount = 1
            rows {
                1 {
                    columns {
                        1 {
                            name = Links
                            colPos = 10
                        }
                        2 {
                            name = Mitte
                            colPos = 11
                        }
                        3 {
                            name = Rechts
                            colPos = 12
                        }
                    }
                }
            }
        }
        icon = EXT:kitt3n_layouts/Resources/Public/Icons/GridElements/ge_3col.png
        flexformDS = FILE:EXT:kitt3n_layouts/Configuration/FlexForm/GridElements/3columns.xml
    }

    4 {
        title = Grid 4 Spalten
        description = Fügt ein Grid-Element mit 4 Spalten ein
        frame = 3
        topLevelLayout = 0
        config {
            colCount = 4
            rowCount = 1
            rows {
                1 {
                    columns {
                        1 {
                            name = Links
                            colPos = 10
                        }
                        2 {
                            name = Mitte Links
                            colPos = 11
                        }
                        3 {
                            name = Mitte Rechts
                            colPos = 12
                        }
                        4 {
                            name = Rechts
                            colPos = 13
                        }
                    }
                }
            }
        }
        icon = EXT:kitt3n_layouts/Resources/Public/Icons/GridElements/ge_4col.png
        flexformDS = FILE:EXT:kitt3n_layouts/Configuration/FlexForm/GridElements/4columns.xml
    }

    5 {
        title = Grid 6 Spalten
        description = Fügt ein Grid-Element mit 6 Spalten ein
        frame = 3
        topLevelLayout = 0
        config {
            colCount = 6
            rowCount = 1
            rows {
                1 {
                    columns {
                        1 {
                            name = Spalte
                            colPos = 10
                        }
                        2 {
                            name = Spalte
                            colPos = 11
                        }
                        3 {
                            name = Spalte
                            colPos = 12
                        }
                        4 {
                            name = Spalte
                            colPos = 13
                        }
                        5 {
                            name = Spalte
                            colPos = 14
                        }
                        6 {
                            name = Spalte
                            colPos = 15
                        }
                    }
                }
            }
        }
        icon = EXT:kitt3n_layouts/Resources/Public/Icons/GridElements/ge_6col.png
        flexformDS = FILE:EXT:kitt3n_layouts/Configuration/FlexForm/GridElements/6columns.xml
    }

    6 {
        title = Grid Wrapper
        description = Fügt ein Grid-Wrapper-Element ein
        frame = 3
        topLevelLayout = 0
        config {
            colCount = 1
            rowCount = 1
            rows {
                1 {
                    columns {
                        1 {
                            name = Inhalt
                            colPos = 10
                        }
                    }
                }
            }
        }

        icon = EXT:kitt3n_layouts/Resources/Public/Icons/GridElements/ge_1col.png
        flexformDS = FILE:EXT:kitt3n_layouts/Configuration/FlexForm/GridElements/wrapper.xml
    }
}