## Basic backend layouts -> EXT:kitt3n_layouts
@import "EXT:kitt3n_layouts/Configuration/TSconfig/BackendLayouts/*.typoscript"

## Custom backend layouts -> EXT:kitt3n_custom
[userFunc = TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('kitt3n_custom')]
    @import "EXT:kitt3n_custom/Configuration/TSconfig/BackendLayouts/*.typoscript"
[global]
