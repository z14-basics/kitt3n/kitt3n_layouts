<?php
(defined('TYPO3_MODE') || defined('TYPO3')) || die('Access denied [kitt3n_layouts ext_localconf.php].');

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$boot = function () {

    if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('kitt3n_layouts')) {

        ## Add PAGE TSConfig
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('@import "EXT:kitt3n_layouts/Configuration/TSconfig/Page/*.typoscript"');

        $rootLineFields = &$GLOBALS["TYPO3_CONF_VARS"]["FE"]["addRootLineFields"];
        if($rootLineFields != '') {
            $rootLineFields .= ',';
        }
        $rootLineFields .= 'backend_layout,backend_layout_next_level';


    }

};

$boot();
unset($boot);